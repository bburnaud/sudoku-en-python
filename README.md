__Sudoku en Python__

Ce programme permet de compléter des sudokus.

Les règles de ce jeu sont dans le fichier correspondant.

Lorsque le programme sera lancé, il sera demandé de choisir entre trois difficultés : facile, moyen et difficile.

La différence entre ces niveaux est le nombre de cases pré-remplies, et, bien évidemment, l´organisation des chiffres.

Ensuite, il sera possible d’entrer la ligne et colonne que vous voulez modifier, ainsi que le chiffre.
Si le chiffre choisi n’est pas dans la grille de correction, ce sera signalé par un message « Erreur »
A l’inverse, si vous entrez le bon chiffre, le message renvoyé sera plus amical.

Si vous complétez la grille en faisant moins de trois erreurs, vous gagnez. 
En revanche, si vous effectuez trois erreurs, vous perdez.

A vous de jouer !




*Réalisé par BURNAUD Bastien*
