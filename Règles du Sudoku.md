__Principe du sudoku__

Le but est de compléter la grille avec des chiffres allant de 1 à 9.
La grille est donc de 9 cases par 9 cases, et est divisée en lignes, colonnes et « carrés ».
Il ne peut y avoir plus d,une fois le même chiffre dans une ligne, colonne ou carré.
Pour pouvoir commencer à remplir la grille, des chiffres sont rentrés de base dans la grille.
Afin de faire une grille plus difficile, et nécéssitant plus de réflexion à terminer, on peut retirer des chiffres de base.
