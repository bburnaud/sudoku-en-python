def grilleVide():
    ligne = [0] * 9
    grille = []
    for k in range(9):
        grille.append(ligne[:])
    return grille

def affGrille(grille):
    conv = {0:"   " , 1:" 1 " , 2:" 2 " , 3:" 3 " , 4:" 4 " , 5:" 5 " , 6:" 6 " , 7:" 7 " , 8:" 8 " , 9:" 9 "}
    a_aff = []
    for g in grille:
        ligne = []
        for l in g:
            ligne.append(conv[l])
        a_aff.append(ligne)
    print("     0)  1)  2)  3)  4)  5)  6)  7)  8)")
    print("   " +"-" * 37)
    for k in range(9):
        l = a_aff[k]
        print("{}) |{}|".format(k , "|".join(l)) )
        print("   " + "-" * 37)
        
def genererGrille():
    liste_diff = ["facile" , "moyen" , "difficile"]
    diff = input("Difficulte ? (facile , moyen ou difficile ) : ")
    diff = diff.lower()
    if diff not in liste_diff:
        diff = "facile"
    if diff == "facile":
        corr1 = [
            [2,6,1,7,3,9,8,4,5],
            [3,7,4,6,8,5,9,1,2],
            [9,5,8,2,1,4,3,6,7],
            [4,9,7,8,5,2,1,3,6],
            [5,3,6,4,7,1,2,8,9],
            [8,1,2,9,6,3,7,5,4],
            [7,8,5,1,2,6,4,9,3],
            [6,2,9,3,4,8,5,7,1],
            [1,4,3,5,9,7,6,2,8]
        ]
        g1 = [
            [0,6,0,7,0,0,0,0,5],
            [0,7,0,6,0,0,9,1,2],
            [9,5,8,2,0,0,3,0,0],
            [0,9,7,8,5,0,0,3,6],
            [0,0,0,4,0,1,0,0,0],
            [0,1,0,0,6,3,7,5,0],
            [0,0,5,0,0,6,4,9,3],
            [6,2,9,0,0,8,0,7,0],
            [1,0,0,0,0,7,0,2,0]
        ]
    elif diff == "moyen":
        g1 = [
            [0,3,0,0,0,8,1,0,6],
            [5,0,0,0,0,3,8,0,0],
            [9,0,8,5,0,0,0,0,2],
            [0,0,6,0,2,5,0,8,0],
            [0,0,0,0,4,0,0,0,0],
            [0,5,0,1,3,0,9,0,0],
            [3,0,0,0,0,2,4,0,1],
            [0,0,7,3,0,0,0,0,8],
            [4,0,5,9,0,0,0,6,0]
        ]
        corr1 = [
            [7,3,2,4,9,8,1,5,6],
            [5,6,1,2,7,3,8,4,9],
            [9,4,8,5,6,1,7,3,2],
            [1,9,6,7,2,5,3,8,4],
            [2,7,3,8,4,9,6,1,5],
            [8,5,4,1,3,6,9,2,7],
            [3,8,9,6,5,2,4,7,1],
            [6,2,7,3,1,4,5,9,8],
            [4,1,5,9,8,7,2,6,3]
        ]
    elif diff == "difficile":
        g1 = [
            [0,0,0,9,0,6,0,3,5],
            [0,0,0,0,0,0,0,0,4],
            [0,8,0,3,1,0,0,6,0],
            [2,5,1,0,3,0,0,7,0],
            [0,0,0,1,0,7,0,0,0],
            [0,3,0,0,2,0,5,8,1],
            [0,6,0,0,9,1,0,5,0],
            [8,0,0,0,0,0,0,0,0],
            [5,2,0,7,0,4,0,0,0]
        ]
        corr1 = [
            [1,7,2,9,4,6,8,3,5],
            [3,9,6,8,7,5,2,1,4],
            [4,8,5,3,1,2,9,6,7],
            [2,5,1,6,3,8,4,7,9],
            [9,4,8,1,5,7,6,2,3],
            [6,3,7,4,2,9,5,8,1],
            [7,6,4,2,9,1,3,5,8],
            [8,1,9,5,6,3,7,4,2],
            [5,2,3,7,8,4,1,9,6]
        ]
    return corr1 , g1
    
def ajouter(grille , corrige , erreurs):
    y = int(input("ligne : "))
    x = int(input("colonne : "))
    chiffre = int(input("chiffre : "))
    if corrige[y][x] != chiffre:
        print("Erreur")
        erreurs += 1
    else:
        grille[y][x] = chiffre
        print("Bien joué ! ")
    return grille , erreurs

def jeu():
    import time
    err = 0
    corr , g = genererGrille()
    while err < 3 and g != corr:
        affGrille(g)
        g , err = ajouter(g , corr , err)
        time.sleep(1)
    if err == 3:
        print("Perdu ! ")
    else:
        print("Bien joué ! ")